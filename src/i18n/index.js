import enUS from './en-US';
import frFR from './fr-FR';

export default {
  'en-us': enUS,
  'fr-fr': frFR,
};
