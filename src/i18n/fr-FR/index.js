export default {
  // job: "Développeur d'applications FullStack junior, Toulouse",
  job: 'Ingénieur DevOps, Toulouse',
  jobRegex: 'd’applications |, toulouse',
  intro: 'Pendant 10 ans, j’ai dû travailler en home office. A chaque nouveau besoin, je me suis adapté, documenté et formé tout en communiquant avec l\'entreprise et mes collègues. L\'objectif: trouver des solutions et assurer un service client de qualité.',
  intro_old: "En 10 ans dans la maintenance, j’ai successivement évolué du poste de technicien jusqu’à celui de responsable de sites. Responsable d'une relation privilégiée avec le client ainsi que le management des sous-traitants sur sites. Je me suis formé à un nouveau domaine, le développement d’applications web. Cette formation m’a permis de renouer avec mes premières expériences dans l’informatique.",
  // <br>Face aux problèmes je trouve toujours une solution.",
  skillsTitle: 'Compétences',
  softSkills: [
    {
      label: 'Façon de travailler',
      subText: [
        'Pensée logique',
        'Esprit critique',
        'Analyser',
        'Faire attention aux détails',
        'Modéliser',
      ],
    },
    {
      label: 'Avec les éléments',
      subText: [
        'Faire face à des situations difficiles',
        'Fiabilité',
        'Force de proposition',
        'Indépendance',
        'Résolution de problème',
      ],
    },

    {
      label: 'Interaction avec les autres',
      subText: [
        'Soutenir',
        'Mener à bien des tâches',
        "L'amélioration des processus ",
        'Enseigner ou former ',
        'Négocier',
        'Résoudre les conflits',
      ],
    },
  ],
  experiences: [
    {
      body: 'Expériences',
      heading: true,
      tag: 'span',
      link: false,
    },
    {
      title: 'DevOps',
      subtitle: '2024',
      compagny: 'AIS',
      link: false,
      location: 'Toulouse',
      strong: ['Terraform', 'Kubernetes', 'AKS', 'Rancher', 'Traefik', 'Grafana', 'Promotheus', 'Kubecost'],
      text: "• Contribuer à l’intégration continue via GitLab du Produit Logiciel industrialisé Apache en intégrant des tests automatisés. Cette chaine devra comporter à la fois l’installation des produits via Ansible sur le IaaS privé mais également la construction des images Docker et leur déploiement dans le registry privé Artifactory pour un deploiement sur GCP, AWS avec terraform.</br>• Réaliser un comparatif Apache et NGINX et identifier l'impact dans la chaine de construction et déploiement du produit</br></br>• Déployer des environnements de test avec Terraform pour permettre la reproductibilité de la solution sur Azure Kubernetes Service (AKS).<br/>• Utiliser Traefik Proxy pour l’intégration de l’authentification à Entra ID.<br/>• Simplifier l'orchestration et la gestion des clusters avec Rancher.<br/>• Observer avec le couple Grafana, Promotheus.<br/>• Optimiser les coûts avec Kubecost.",    },
    {
      title: 'DevOps',
      subtitle: '2021 - 2023',
      compagny: 'AGORAVITA',
      link: false,
      location: 'Toulouse',
      strong: ['VMWARE', 'Gitlab_CI', 'Ansible', 'Vault', 'Boundary', 'Linux', 'hardening'],
      text: "Mise en place du déploiement automatique et configuration des Vm sur l'environnement VMWARE avec Ansible. Consolidation de l'intégration continue via Gitlab_CI. Automatismes de gestion des certificats SSL et notifications Teams. Mise en service de Grafana pour un monitoring du Datacenter plus \"User Friendly\". Support Linux N3. Test des solutions HashiCorp Vault et Boundary et hardening suivant recommandations ANSSI et CIS",
    },
    {
      title: 'Développeur d’application FullStack',
      subtitle: '2020',
      compagny: 'Stage 3 mois',
      link: false,
      location: 'Toulouse',
      strong: ['Node.js', 'Gitlab_CI', 'Grafana', 'mysql'],
      text: "En relation avec le devOps, mise en place d’un environnement Node.js et d’intégration continue avec Gitlab_CI.<br/>Exploration de données avec Grafana.<br/>Développement d'algorithmes de traitement de données MYSQL et d’alertes",
    },
    {
      title: 'Responsable de sites photovoltaïques',
      subtitle: '2016 - 2019',
      compagny: 'BAYWA R.E. France',
      link: false,
      location: 'Toulouse',
      strong: ['php', 'Modbus rtu/tcp'],
      text: 'Organisation, gestion de la maintenance et du dépannage en toute autonomie. Gestion de proximité avec le client. Développement en PHP d’applications de communication avec des automates utilisant le protocole Modbus RTU/TCP pour la consultation des données et la prise de décisions',
    },
    {
      title: 'Gérant en maintenance photovoltaïque',
      subtitle: '2013 - 2015',
      compagny: 'DÉMESSI SARL',
      link: false,
      location: 'Naucelle',
      strong: ['dolibarr', 'api googlemaps'],
      text: 'Intégration d’un ERP/CRM Dolibarr.Développement de modules:<br/>• Gestion des itinéraires, tournées avec l’API GoogleMaps<br/>• Calculs automatiques des frais de déplacements avec intégration dans le devis',
    },
    {
      title: 'Technicien SAV itinérant en photovoltaïque',
      subtitle: '2010 - 2013',
      compagny: 'TENESOL',
      link: false,
      location: 'Toulouse',
      text: 'Responsabilité de 80 sites de production sur le quart Sud-Ouest.Configuration, mises à jour et dépannage de webblog / datalogger. Expertise de panneaux défectueux.Gestion du stock de pièces détachées, organisation du planning et des déplacements en toute autonomie.',
    },
    {
      title: 'Électricien',
      subtitle: '2006 - 2010',
      compagny: 'Missions intérimaires',
      link: false,
      location: 'Rodez',
      // text: 'Expérience pro',
    },
    {
      title: 'Opérateur, tourneur, fraiseur sur CN',
      subtitle: '2004 - 2005',
      compagny: 'Missions intérimaires',
      link: false,
      location: 'Rodez',
      // text: 'Expérience pro',
    },
    {
      title: 'Technicien HOT LINE / MICRO',
      subtitle: '2000 - 2003',
      compagny: 'Missions pour SSII',
      link: false,
      location: 'Paris',
      // text: '• Assistance utilisateurs<br/>• Installation de postes de travail',
    },
  ],
  education: [
    {
      body: 'Formations',
      heading: true,
      tag: 'span',
      link: false,
    },
        {
      title: 'Certification VMWare Double VCP',
      subtitle: '2022 - 2023',
      link: false,
      // compagny: 'INP - ENSEEIHT',
      // location: 'Toulouse',
      // program: 'Programme de formation',
      // link: 'pdf/FullStack_Programme.pdf',
      // strong: ['Javascript', 'NodeJS', 'Python', 'HTML5', 'CSS3', 'Vue.js', 'REACT', 'Django', 'AGILE', 'GIT', 'UNIX', 'django', 'réseaux'],
      text: 'Data Center Virtualization & Network Virtualization 2023',
    },
    {
      title: 'Diplôme universitaire Développeur FullStack',
      subtitle: '2019 - 2020',
      compagny: 'INP - ENSEEIHT',
      location: 'Toulouse',
      program: 'Programme de formation',
      link: 'pdf/FullStack_Programme.pdf',
      strong: ['Javascript', 'NodeJS', 'Python', 'HTML5', 'CSS3', 'Vue.js', 'REACT', 'Django', 'AGILE', 'GIT', 'UNIX', 'django', 'réseaux'],
      text: 'Répondre à une demande client à partir d’un cahier des charges établi<br>Programmer et développer avec les langages Javascript, NodeJS et Python<br>Concevoir, actualiser et modulariser une application<br>Développer une application en front- end (HTML5, CSS3, Vue.js, React)<br>Développer une application en back - end (NodeJS, Django)<br>Gérer un projet avec une méthode de développement AGILE et des outils de partage GIT<br>Développer sous un système UNIX<br>Configurer l\'environnement réseaux',
    },
    {
      title: 'Formation PHP - MYSQL intermédiaire',
      subtitle: '2017',
      compagny: 'DAWAN',
      location: 'Toulouse',
      program: 'Programme de formation',
      link: 'pdf/Dawan_Programme.pdf',
      strong: ['POO', 'PHP', 'PDO', 'mysqli', 'mysql'],
      text: 'Maîtriser les fondamentaux de la programmation orientée objet PHP (POO)<br> Utilisation des bibliothèques (MySQLi, PDO). Gestion de connexion. Générer une requête, injecter des paramètres. Principes de transactions. Erreurs et exceptions',
    },

    {
      title: 'CAP/BEP Électricien d’équipement - SST',
      subtitle: '2005 - 2006',
      compagny: 'A.F.P.A.',
      location: 'Rodez',
      link: false,
      // text: 'education pro'

    },
    {
      title: 'BAC STI Électronique',
      subtitle: '1998',
      compagny: 'Lycée A.MONTEIL',
      location: 'Rodez',
      link: false,
      // text: 'education pro',
    },
  ],
};
