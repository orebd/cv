/* eslint-disable indent */
export default
    [
        {
            label: 'Debian', value: 7, favori: true, group: 'system', href: 'https://www.debian.org/',
        },
        {
 label: 'Vcenter', value: 6, group: 'system', href: 'https://www.vmware.com/fr/products/vcenter.html',
},
        {
 label: 'NSX-T', value: 5, group: 'system', href: 'https://www.vmware.com/fr/products/nsx.html',
},
        { label: 'Gitlab CI', value: 6, group: 'saas' },
        {
 label: 'Ansible', value: 6, group: 'system', href: 'https://www.ansible.com/',
},

        {
 label: 'Nessus', value: 5, group: 'system', href: 'https://fr.tenable.com/',
},
        {
 label: 'Docker', value: 5, group: 'system', href: 'https://www.docker.com/',
},
       // {
       //     label: 'Node.js', value: 6, favori: true, group: 'environnement',
       // },
                {
            label: 'Kubernetes', value: 3, favori: false, group: 'environnement',
        },
        { label: 'Rancher', value: 3, group: 'environnement' },
        { label: 'Traefik', value: 3, group: 'environnement' },
        { label: 'Azure Kubernetes Service(AKS)', value: 3, group: 'environnement' },
        // { label: 'PHP', value: 7, group: 'environnement' },
        // { label: 'Python', value: 3, group: 'environnement' },
        // { label: 'Mysql / MariaDB', value: 7, group: 'db' },
        // {
            // label: 'Vue.js', value: 6, favori: true, group: 'framework', href: 'https://vuejs.org/',
        // },
//         {
//  label: 'Quasar', value: 6, favori: true, group: 'framework', href: 'https://quasar.dev/',
// },
        // { label: 'Git', value: 6, group: 'versionning' },

//         {
//  label: 'Heroku', value: 5, group: 'deployement', href: 'https://www.heroku.com/',
// },
//         {
//             label: 'Jest', value: 5, favori: true, group: 'test', href: 'https://jestjs.io/fr/',
//         },

        // { label: 'Electron.js', group: 'interest' },
        // { label: 'Capacitor', group: 'interest' },
        // { label: 'HashiCorp Vault', group: 'interest', href: 'https://www.hashicorp.com/products/vault' },
        { label: 'Bastion HashiCorp Boundary', group: 'interest', href: 'https://www.hashicorp.com/products/boundary' },
    ];
