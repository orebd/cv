const routes = [
  {
    path: '/skills',
    component: () => import(/* webpackChunkName: "skills" */ 'pages/Skills.vue'),
  },
  {
    path: '/experiences',
    component: () => import(/* webpackChunkName: "education" */ 'pages/Experiences.vue'),
  },
  {
    path: '/education',
    component: () => import(/* webpackChunkName: "education" */ 'pages/Education.vue'),
  },
  {
    path: '/contact',
    component: () => import(/* webpackChunkName: "education" */ 'pages/Contact.vue'),
  },
  {
    path: '/print/:jobTitle?',
    component: () => import(/* webpackChunkName: "Print" */ 'pages/Print.vue'),
  },
  {
    path: '/:jobTitle?',
    component: () => import('pages/Index.vue'),
  },
  {
    path: '*',
    component: () => import(/* webpackChunkName: "404" */ 'pages/Error404.vue'),
  },
];

export default routes;
